# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    EDI_code = fields.Char('EDI Code')


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    EDI_code = fields.Function(fields.Char('EDI Code'), 'get_edi_code',
        searcher='search_edi_code')

    def get_edi_code(self, name):
        return self.template.EDI_code

    @classmethod
    def search_edi_code(cls, name, clause):
        return [('template.' + clause[0],) + tuple(clause[1:])]


class Reference(metaclass=PoolMeta):
    __name__ = 'product.cross_reference'

    product_uom_category = fields.Function(
        fields.Many2One('product.uom.category', 'UOM Category'),
        'on_change_with_product_uom_category')
    edi_uom = fields.Many2One('product.uom', 'EDI UOM',
        domain=[('category', '=', Eval('product_uom_category'))],
        depends=['product_uom_category'])

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        super().__register__(module_name)
        table.drop_column('EDI_code')

    @fields.depends('product')
    def on_change_with_product_uom_category(self, name=None):
        return self.product and self.product.default_uom.category.id
