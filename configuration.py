# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelSingleton, ModelView, fields, Unique
from trytond.pool import Pool
from trytond.pyson import Id
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserError
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    """Edocument Configuration"""
    __name__ = 'edocument.configuration'

    edocument_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Electronic Document Sequence',
            required=True, domain=[
                ('sequence_type', '=', Id('edocument_edifact',
                    'sequence_type_edocument'))
            ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'edocument_sequence':
            return pool.get('edocument.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)


class ConfigurationSequence(ModelSQL, CompanyValueMixin):
    """Edocument Configuration Sequence"""
    __name__ = 'edocument.configuration.sequence'

    edocument_sequence = fields.Many2One(
        'ir.sequence', 'Electronic Document Sequence', required=True,
        domain=[
            ('sequence_type', '=', Id('edocument_edifact',
                'sequence_type_edocument'))
        ])

    @classmethod
    def default_edocument_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('edocument_edifact', 'sequence_edocument')
        except KeyError:
            return None


class ConfigurationPath(ModelSQL, ModelView):
    """Electronic Document configuration path"""
    __name__ = 'edocument.configuration.path'

    message_type = fields.Selection([], 'Document', required=True)
    path = fields.Char('Path')
    error_path = fields.Char('Errors path')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('messsage_type_uniq', Unique(t, t.message_type),
                'edocument_edifact.msg_message_type_unique'),
        ]

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        EdocumentConfiguration = pool.get('edocument.configuration')

        super().__register__(module_name)

        sql_table = cls.__table__()
        edocument_table = EdocumentConfiguration.__table_handler__(module_name)
        edocument_config_table = EdocumentConfiguration.__table__()
        cursor = Transaction().connection.cursor()

        if (edocument_table.column_exist('export_path')
                and edocument_table.column_exist('error_path')):
            cursor.execute(*edocument_config_table.select(
                edocument_config_table.create_date,
                edocument_config_table.write_date,
                edocument_config_table.export_path,
                edocument_config_table.error_path))
            sql_values = cursor.fetchall()
            message_type = cls.get_message_type_values()
            if message_type and sql_values:
                cursor.execute(*sql_table.select(
                    sql_table.message_type))
                types = set(message_type) - set(
                    msg[0] for msg in cursor.fetchall())
                for type_ in list(types):
                    sql_values_insert = sql_values[0] + (type_,)
                    cursor.execute(*sql_table.insert(columns=[
                        sql_table.create_date,
                        sql_table.write_date,
                        sql_table.path,
                        sql_table.error_path,
                        sql_table.message_type
                        ],
                        values=[sql_values_insert]))

    @classmethod
    def get_message_type_values(cls):
        return [value[0] for value in cls.fields_get(
            ['message_type'])['message_type']['selection']]

    @classmethod
    def _get_path(cls, message_type, check_values=[]):
        config, = cls.search([
            ('message_type', '=', message_type)]) or [None]
        if (not config or any(not getattr(config, fvalue, None)
                for fvalue in check_values)):
            raise UserError(
                gettext('edocument_edifact.msg_message_type_path_required'),
                message_type=message_type)
        return config
