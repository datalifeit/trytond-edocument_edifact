==========================
Edocument Edifact Scenario
==========================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install edocument_edifact::

    >>> config = activate_modules('edocument_edifact')
